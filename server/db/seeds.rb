#***********************************************************************************************************************************#
#                                                       Notice                                                                      #
#                                 Before run the rails db:seed, make sure you run rails db:reset                                    #
#                                     Or run with 2 commands: rails db:drop and rails db:reset                                      #
#***********************************************************************************************************************************#
abort "only run seeds in development or test" unless Rails.env.development? or Rails.env.test?
require "faker"

User.delete_all
Room.destroy_all
Role.destroy_all
Formation.destroy_all
FormationSession.destroy_all
FormationAttendance.destroy_all
Category.destroy_all
FormationCategory.destroy_all

ActiveRecord::Base.connection.reset_pk_sequence!(User)
ActiveRecord::Base.connection.reset_pk_sequence!(Room)
ActiveRecord::Base.connection.reset_pk_sequence!(Role)
ActiveRecord::Base.connection.reset_pk_sequence!(Formation)
ActiveRecord::Base.connection.reset_pk_sequence!(FormationSession)
ActiveRecord::Base.connection.reset_pk_sequence!(FormationAttendance)
ActiveRecord::Base.connection.reset_pk_sequence!(Category)
ActiveRecord::Base.connection.reset_pk_sequence!(FormationCategory)
# User.reset_pk_sequence!
# Room.reset_pk_sequence!
# Role.reset_pk_sequence!
# Formation.reset_pk_sequence!
# FormationSession.reset_pk_sequence!
# FormationAttendance.reset_pk_sequence!
# Category.reset_pk_sequence!
# FormationCategory.reset_pk_sequence!

Role.create(name: "student")
Role.create(name: "admin")
Role.create(name: "teacher")

# Create some fake user
50.times do
  user = User.new(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.unique.email,
    age: rand(18..99),
    role: Role.first,
    password: "1234567",
    is_validated: true,
  )
  user.username = user.first_name.downcase + "-" + user.last_name.downcase
  user.save
end
puts "50 users created!"

# Create an user for dev

User.create(
  first_name: "admin",
  last_name: "0",
  email: "admin@yopmail.com",
  age: rand(18..99),
  password: "1234567",
  role: Role.find_by(name: "admin"),
  is_validated: true,
)

puts "admin@yopmail.com created!"

User.create(
  first_name: "admin",
  last_name: "1",
  email: "admin1@yopmail.com",
  username: "admin1",
  age: rand(18..99),
  password: "1234567",
  role: Role.find_by(name: "admin"),
  is_validated: true,
)

User.create(
  first_name: "Student",
  last_name: "1",
  username: "student1",
  email: "student1@yopmail.com",
  password: "1234567",
  age: rand(18..99),
  role: Role.first,
  is_validated: true,
)
puts "student1@yopmail.com created!"

User.create(
  first_name: "teacher",
  last_name: "1",
  email: "teacher1@yopmail.com",
  username: "teacher1",
  age: rand(30..99),
  password: "1234567",
  role: Role.last,
  is_validated: true,
)
puts "student1@yopmail.com created!"

10.times do
  Formation.create(title: "Super formation",
                   description: Faker::Lorem.words(number: 20),
                   teacher_id: User.last.id)
end
puts "10 super formation created!"

10.times do |i|
  Room.create(name: "Room formation #{i}")
end
puts "10 rooms created"

languages = ["Javascript", "TypeScript", "C#", "Python", "Ruby", "Golang", "Java", "Rust", "HTML-CSS"]

languages.each do |name|
  Room.create(name: name)
end

frameworks = ["Nodejs", "React", "Vue", "Angular", "Nestjs", "Nextjs", "Vitejs", "Bootstrap", "TailwindCSS", "RubyOnRails", "Python-Django", "Python-Flask", ".Net-core", "Deno", "Nuxtjs"]

frameworks.each do |name|
  Room.create(name: name)
end

100.times do |i|
  FormationSession.create(room: Room.all.sample,
                          formation: Formation.all.sample,
                          start_date: Time.now,
                          end_date: Time.now + 3600 * 24, capacity: rand(1..20))
end

100.times do |i|
  FormationAttendance.create(user: User.all.sample,
                             formation: Formation.all.sample,
                             formation_session: FormationSession.all.sample)
end

10.times do |i|
  Category.create(name: "category test #{i}")
end

languages.each do |name|
  Category.create(name: name)
end

puts "10 categories created"

40.times do |i|
  FormationCategory.create(category: Category.all.sample,
                           formation: Formation.all.sample)
end
puts "Done!"
