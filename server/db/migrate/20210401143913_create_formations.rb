class CreateFormations < ActiveRecord::Migration[6.0]
  def change
    create_table :formations, if_not_exists: true do |t|
      t.string :title
      t.string :description
      t.belongs_to :teacher, index: true

      t.timestamps
    end
  end
end
