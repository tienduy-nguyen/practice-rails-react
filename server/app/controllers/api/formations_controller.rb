class Api::FormationsController < ApplicationController
  before_action :set_formation, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, only: [:create, :update, :destroy]

  # GET /formations
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    @count = Formation.count
    @formations = Formation.offset((page - 1) * limit).limit(limit)

    formations_json = ActiveModel::SerializableResource.new(@formations)

    render json: { count: @count, formations: formations_json }
  end

  # GET /formations/:id
  def show
    render json: @formation
  end
# POST /formations
  def create
    @formation = Formation.new(formation_params)
    if @formation.save
      render json: @formation, status: :created, location: @api_formation
    else
      render json: @formation.errors, status: :unprocessable_entity
    end
  end

  # PUT /formations/:id
  def update
    if @formation.update(formation_params)
      render json: @formation
    else
      render json: @formation.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @formation.destroy
    render json: { destroy: true }, status: 200
  end

  private

  def set_formation
    @formation = Formation.find(params[:id])
  end

  def is_admin
    if current_user && current_user&.role&.name == "admin"
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: 401
    end
  end

  def formation_params
    params.require(:formation).permit(:title, :description, :teacher_id)
  end
end
