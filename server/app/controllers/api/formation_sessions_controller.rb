class Api::FormationSessionsController < ApplicationController
  before_action :set_formation_session, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, only: [:create, :update, :destroy]

  # GET /formations
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    formation_id = params[:formation_id]
    if formation_id
      @formation_sessions = Formation.find(formation_id: formation_id).formation_sessions
      @count = @formation_sessions.length
      offset = (page - 1) * limit
      @formation_sessions = offset > 0 ? @formation_sessions.drop(offset) : @formation_sessions
      @formation_sessions = @formation_sessions.first(limit)
    else
      @count = Formation.count
      @formation_sessions = FormationSession.offset((page - 1) * limit).limit(limit)
    end

    formation_sessions = ActiveModel::SerializableResource.new(@formation_sessions)
    render json: { count: @count, formation_sessions: formation_sessions }
  end

  # GEt /formation_sessions/:id # GET
  def show
    render json: @formation_session
  end

  # POST /formation_sessions
  def create
    @formation_session = FormationSession.new(formation_session_params)
    # set and check errors if not found
    begin
      @formation_session.formation = Formation.find(params[:formation_id])
      @formation_session.room = Room.find(params[:room_id])
      @formation_session.save
      render json: @formation_session, status: :created, location: @api_formation_session
    rescue => exception
      render json: exception, status: :unprocessable_entity
    end
  end

  # PUT /formation_sessions/:id
  def update
    if @formation_session.update(formation_session_params)
      render json: @formation_session
    else
      render json: @formation_session.errors, status: :unprocessable_entity
    end
  end

  # DELETE /formation_sessions/:id
  def destroy
    @formation_session.destroy
    render json: { destroy: true }, status: 200
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_formation_session
    @formation_session = FormationSession.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def formation_session_params
    params.require(:formation_session).permit(
      :start_date, :end_date, :capacity, :room_id, :formation_id
    )
  end

  def is_admin
    if current_user && current_user&.role&.name == "admin"
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: :unauthorized
    end
  end
end
