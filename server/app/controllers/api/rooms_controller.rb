$LOAD_PATH << "app"
require "utils/query_string"

class Api::RoomsController < ApplicationController
  before_action :set_room, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, except: [:index, :show]

  # GET /rooms
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    formation_id = params[:formation_id]
    formation_session_id = params[:formation_session_id]
    if formation_session_id
      begin
        @rooms = FormationSession.find(formation_session_id)&.room
        @count = 1
        rooms_json = ActiveModel::SerializableResource.new(@rooms)
        return render json: { count: @count, rooms: rooms_json }
      rescue => exception
        return render json: { error: exception,
                              status: 404 }, status: 404
      end
    end
    if formation_id
      @rooms = Formation.find(formation_id).rooms
      @count = @rooms.length
      offset = (page - 1) * limit
      @rooms = offset > 0 ? @rooms.drop(offset) : @rooms
      @rooms = @rooms.first(limit)
    else
      @rooms = Room.offset((page - 1) * limit).limit(limit)
      @count = Room.count
    end

    rooms_json = ActiveModel::SerializableResource.new(@rooms)
    render json: { count: @count, rooms: rooms_json }
  end

  # GET /rooms/:id
  def show
    render json: @room
  end

  # POST /rooms
  def create
    @room = Room.new(room_params)

    if @room.save
      render json: @room, status: :created, location: @api_room
    else
      render json: @room.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rooms/1
  def update
    if @room.update(room_params)
      render json: @room
    else
      render json: @room.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rooms/:id
  def destroy
    @room.destroy
    render json: { destroy: true }, status: 200
  end

  private

  def set_room
    @room = Room.find(params[:id])
  end

  def room_params
    params.require(:room).permit(:name)
  end

  def is_admin
    if current_user & current_user&.role&.name == "admin" then return true end
    render json: { error: "UnAuthorized", status: 401 }, status: 401
  end
end
