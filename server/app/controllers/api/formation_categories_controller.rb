class Api::FormationCategoriesController < ApplicationController
  before_action :set_formation_category, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, only: [:create, :update, :destroy]

  # GET /formation_categories
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    formation_id = params[:formation_id]
    if formation_id
      @formation_categories = Formation.find(formation_id: formation_id).formation_categories
      @count = @formation_categories.length
      offset = (page - 1) * limit
      @formation_categories = offset > 0 ? @formation_categories.drop(offset) : @formation_categories
      @formation_categories = @formation_categories.first(limit)
    else
      @formation_categories = FormationCategory.offset((page - 1) * limit).limit(limit)
      @count = FormationCategory.count
    end

    formation_categories_json = ActiveModel::SerializableResource.new(@formation_categories)
    render json: { coutn: @count, formation_categories: formation_categories_json }
  end

  # GET /formation_categories/:id
  def show
    render json: @formation_category
  end

  # POST /formation_categories
  def create
    begin
      @formation_category = FormationCategory.new(formation_category_params)
      @formation_category.category = Category.find(params[:category_id])
      @formation_category.formation = Formation.find(params[:formation_id])
      @formation_category.save
      render json: @formation_category, status: :created, location: @api_formation_category
    rescue => exception
      render json: exception, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /formation_categories/:id
  def update
    begin
      # Check if category_id, and formation_id input exist in database
      @formation_category.category = Category.find(params[:category_id])
      @formation_category.formation = Formation.find(params[:formation_id])
      @formation_category.save
      render json: @formation_category
    rescue => exception
      render json: exception, status: :unprocessable_entity
    end
  end

  # DELETE /formation_categories/1
  def destroy
    @formation_category.destroy
    render json: { destroy: true }, status: 200
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_formation_category
    @formation_category = FormationCategory.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def formation_category_params
    params.require(:formation_category).permit(:formation_id, :category_id)
  end

  def is_admin
    if current_user && current_user.role.name == "admin"
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: :unauthorized
    end
  end
end
