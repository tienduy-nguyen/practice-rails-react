class Api::CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, only: [:create, :update, :destroy]

  # GET /categories, using query formation_id, limit, page
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i

    if params[:formation_id]
      @categories = Array.new
      formation_id = params[:formation_id]

      category_formations = FormationCategory
        .where(formation_id: formation_id)
        .offset((page - 1) * limit).limit(limit)

      category_formations.each do |item|
        @categories.push(item.category)
      end
      @count = FormationCategory.where(formation_id: formation_id).count
    else
      @categories = Category.offset((page - 1) * limit).limit(limit)
      @count = Category.count
    end

    render json: { count: @count, categories: @categories }
  end

  # Get /categories/:id
  def show
    @formations = Array.new
    category_formations = FormationCategory.where(category_id: @category.id)
    category_formations.each do |item|
      @formations.push(item)
    end
    if @formations.blank? then return render json: { error: "Not found", status: 404 }, status: 404 end
    render json: @formations
  end

  # POST /categories
  def create
    @category = Category.new(category_params)

    if @category.save
      render json: @category, status: :created, location: @api_category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /categories/:id
  def update
    if @category.update(category_params)
      render json: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
    return render json: { destroy: true }, status: 200
  end

  # ---------------private methods-----------------------------
  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end

  def is_admin
    if current_user && current_user&.role&.name == "admin" then return true end
    render json: { error: "UnAuthorized", status: 401 }, status: 401
  end
end
