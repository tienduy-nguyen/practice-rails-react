class Api::FormationAttendancesController < ApplicationController
  before_action :set_formation_attendance, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :can_i_destroy, only: [:destroy]
  before_action :can_i_update, only: [:update]
  before_action :is_validated, except: [:index, :show]

  # GET /formation_attendances
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    user_id = params[:user_id]
    formation_id = params[:formation_id]

    # find by user
    if user_id
      @count = FormationAttendance.where(user_id: user_id).count
      @formation_attendances = FormationAttendance
        .where(user_id: user_id)
        .offset((page - 1) * limit).limit(limit)
      formation_attendaces_json = ActiveModel::SerializableResource.new(@formation_attendances)
      return render json: { count: @count, formation_attendances: formation_attendaces_json }
    end

    # find by formation
    if formation_id
      @count = FormationAttendance.where(formation_id: formation_id).count
      @formation_attendances = FormationAttendance
        .where(formation_id: formation_id)
        .offset((page - 1) * limit).limit(limit)
      formation_attendaces_json = ActiveModel::SerializableResource.new(@formation_attendances)
      return render json: { count: @count, formation_attendances: formation_attendaces_json }
    end

    @count = FormationAttendance.count
    @formation_attendances = FormationAttendance
      .offset((page - 1) * limit).limit(limit)

    formation_attendaces_json = ActiveModel::SerializableResource.new(@formation_attendances)
    return render json: { count: @count, formation_attendances: formation_attendaces_json }
  end

  # GET /formation_attendances/:id
  def show
    render json: @formation_attendance
  end

  # POST /formation_attendances
  def create
    begin
      @formation_attendance = FormationAttendance.new(formation_attendance_params)
      @formation_attendance.user_id = current_user.id
      @formation_attendance.save
      render json: @formation_attendance, status: :created, location: @api_formation_attendance
    rescue => exception
      render json: exception, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /formation_attendances/:id
  def update
    begin
      # if is teacher, update mark params
      if params[:mark] && current_user&.role&.name == "teacher"
        @formation_attendance.update(formation_attendance_params)
        return render json: @formation_attendance
      end
      # if admin want to change an attendance of one user
      @formation_attendance.formation = Formation.find(params[:formation_id])
      @formation_attendance.formation_session = FormationSession.find(params[:formation_session])
      @formation_attendance.user = User.find(params[:user_id])
      @formation_attendance.save
      return render json: @formation_attendance
    rescue => exception
      render json: exception, status: :unprocessable_entity
    end
    if @formation_attendance.update(formation_attendance_params)
      render json: @formation_attendance
    else
      render json: @formation_attendance.errors, status: :unprocessable_entity
    end
  end

  # DELETE /formation_attendances/1
  def destroy
    @formation_attendance.destroy
    render json: { canceled: true }, status: 200
  end

  # ---------------------private methods--------------------------
  private

  def set_formation_attendance
    @formation_attendance = FormationAttendance.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def formation_attendance_params
    if current_user && current_user.role.name == "teacher"
      params.require(:formation_attendance).permit(:mark)
    else
      params.require(:formation_attendance).permit(
        :formation_id, :formation_session_id, :user_id
      )
    end
  end

  # Check right to cancel attend formation of one user
  def can_i_destroy
    if current_user && (current_user.role.name == "admin" || current_user.id == @formation_attendance.user_id)
      return true
    else
      render json: "You cannot cancel this registration if you are not an administrator or the registrant.", status: :unauthorized
    end
  end

  def can_i_update
    if current_user&.role&.name == "admin" || current_user&.role&.name == "teacher"
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: :unauthorized
    end
  end

  def is_validated
    if current_user && User.find(current_user&.id).is_validated
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: :unauthorized
    end
  end
end
