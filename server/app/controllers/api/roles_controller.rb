class Api::RolesController < ApplicationController
  before_action :set_role, only: [:show, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :is_admin, except: [:index]

  # GET/roles
  def index
    limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
    page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
    if params[:user_id]
      @roles = Role.left_joins(:users)
      @count = Role.left_joins(:users).count
    else
      @roles = Role.offset((page - 1) * limit).limit(limit)
      @count = Role.count
    end

    roles_json = ActiveModel::SerializableResource.new(@roles)
    render json: { count: @count, roles: roles_json }
  end

  # GET /roles/:id
  def show
    render json: @role
  end

  # POST /rooms
  def create
    @role = Role.new(role_params)
    if @role.save
      render json: @role, status: :created, location: @api_role
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  # PUT /rooms/:id
  def update
    if @role.update(role_params)
      render json: @role
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  # DELETE /roles/:id
  def destroy
    @role.destroy
    render json: { destroy: true }, status: 200
  end

  private

  def set_role
    @role = Role.find(params[:id])
  end

  def is_admin
    if current_user && current_user&.role&.name == "admin"
      return true
    else
      render json: { error: "UnAuthorized", status: 401 }, status: 401
    end
  end

  def role_params
    params.require(:role).permit(:name)
  end
end
