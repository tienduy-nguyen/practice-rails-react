class Room < ApplicationRecord
  has_many :formation_sessions
  has_many :formation_attendances, through: :formation_sessions
  has_many :formations, through: :formation_sessions
  has_many :users, through: :formation_sessions

  validates :name, presence: true
end
