class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist

  # Active record callback
  before_save :update_username, if: :username_changed?

  # Assosication
  has_many :images

  # Validation
  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false },
            format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, message: "Email adress please" }

  validates :password, length: { minimum: 3 }, presence: true, on: :create
  validates :password, length: { minimum: 3 }, presence: true, on: :update, if: :encrypted_password_changed?

  def update_username
    if self.username.blank?
      self.username = self.first_name.downcase + "-" + self.last_name.downcase
    end
  end

  # Associations
  belongs_to :role
  has_many :formation_attendances
  has_many :formations, through: :formation_attendances
  has_many :formation_sessions, through: :formation_attendances
  has_many :rooms, through: :formation_sessions
  has_many :formations, foreign_key: :teacher_id, dependent: :destroy
end
