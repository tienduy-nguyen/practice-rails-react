class Category < ApplicationRecord
  #Assiosiations
  has_many :formation_categories
  has_many :formations, through: :formation_categories

  # Validation
  validates :name, presence: true
end
