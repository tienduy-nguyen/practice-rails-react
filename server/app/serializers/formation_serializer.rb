class FormationSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
  has_one :teacher
  has_many :rooms
  has_many :formation_sessions
  has_many :categories
  # has_many :users
end
