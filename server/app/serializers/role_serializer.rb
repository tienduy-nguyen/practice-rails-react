class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :formation_sessions
end
