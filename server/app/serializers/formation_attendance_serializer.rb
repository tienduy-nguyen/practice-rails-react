class FormationAttendanceSerializer < ActiveModel::Serializer
  attributes :id
  has_one :user
  has_one :formation_session
end
