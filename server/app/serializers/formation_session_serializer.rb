class FormationSessionSerializer < ActiveModel::Serializer
  attributes :id
  has_one :room
  has_one :formation
end
