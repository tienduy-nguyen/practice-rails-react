def is_admin
  if current_user & current_user&.role&.name == "admin" then return true end
  render json: { error: "UnAuthorized", status: 401 }, status: 401
end
