def get_limit
  limit = params[:limit].to_i <= 0 ? 25 : params[:limit].to_i
  return limit
end

def get_page
  page = params[:p].to_i <= 0 ? 1 : params[:p].to_i
end
