Rails.application.routes.draw do
  get 'formations/formation_sessions'
  default_url_options :host => "http://localhost:4120/"

  namespace :api, defaults: { format: :json } do
    get "/auth/me", to: "users#me", as: "me"
    resources :users
    resources :categories
    resources :rooms

    resources :formation_attendances
    resources :formation_categories
    resources :roles
    resources :formation_sessions

    resources :users do
      resources :formation_attendances, only: [:index]
      resources :roles, only: [:index]
    end

    resources :formations do
      resources :formation_sessions, only: [:index, :create] do
        resources :rooms, only: [:index]
      end
      resources :users, only: [:index]
      resources :rooms, only: [:index]
      resources :categories, only: [:index]
      resources :formation_attendances, only: [:index]
    end
  end

  devise_for :users,
             defaults: { format: :json },
             path: "",
             path_names: {
               sign_in: "api/auth/login",
               sign_out: "api/auth/logout",
               registration: "api/auth/register",
             },
             controllers: {
               sessions: "sessions",
               registrations: "registrations",
             }
end
