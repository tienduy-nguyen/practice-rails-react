require 'rails_helper'

RSpec.describe "Formations", type: :request do
  describe "GET /formation_sessions" do
    it "returns http success" do
      get "/formations/formation_sessions"
      expect(response).to have_http_status(:success)
    end
  end

end
