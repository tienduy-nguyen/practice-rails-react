import React from 'react';
import { LoadingIndicator } from 'src/app/components/Generals/LoadingIndicator';
import { lazyLoad } from 'src/app/utils/loadable';

export const NotFoundPage = lazyLoad(
  () => import('./index'),
  module => module.NotFoundPage,
  {
    fallback: <LoadingIndicator />,
  },
);
