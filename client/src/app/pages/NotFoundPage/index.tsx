import React from 'react';
import { useHistory } from 'react-router-dom';
import { MainLayout } from 'src/app/components/Layouts/MainLayout';

export const NotFoundPage = () => {
  const history = useHistory();
  return (
    <MainLayout>
      <div className="text-center mt-4">
        <h1>404 - That page does not seem to exist...</h1>
        <div>
          <iframe
            src="https://giphy.com/embed/l2JehQ2GitHGdVG9y"
            width="480"
            height="362"
            frameBorder="0"
            allowFullScreen
          ></iframe>
        </div>
        <div>
          <button className="btn btn-primary mt-4" onClick={() => history.goBack()}>
            Go home
          </button>
        </div>
      </div>
    </MainLayout>
  );
};
