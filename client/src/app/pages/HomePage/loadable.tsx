import React from 'react';
import { LoadingIndicator } from 'src/app/components/Generals/LoadingIndicator';
import { lazyLoad } from 'src/app/utils/loadable';

export const HomePage = lazyLoad(
  () => import('./index'),
  module => module.HomePage,
  {
    fallback: <LoadingIndicator />,
  },
);
