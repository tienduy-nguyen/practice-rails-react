import React from 'react';

interface Props {}
export const MainLayout: React.FC<Props> = ({ children }) => {
  return (
    <>
      <main className="py-3" style={{ minHeight: '85vh' }}>
        <h1>Body</h1>
        {children}
      </main>
    </>
  );
};
