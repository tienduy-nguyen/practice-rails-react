import React, { useState } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useAppDispatch } from 'src/store';

export const NavBar = () => {
  const [user] = useState(null);
  const dispatch = useAppDispatch();

  const logoutUser = () => {
    await dispatch(logout());
  };
  let authGroupButtons = (
    <>
      <Link to="/login">
        <Nav.Link>
          <i className="fas fa-sign-in-alt"></i>
        </Nav.Link>
      </Link>
      <>
        <Link to="/register">
          <Nav.Link>
            <i className="fas fa-user-plus"></i>
          </Nav.Link>
        </Link>
      </>
    </>
  );
  if (user) {
    authGroupButtons = (
      <>
        <NavDropdown title={user?.fullName || user?.username} id="username">
          <Link to="/profile">
            <NavDropdown.Item>Profile</NavDropdown.Item>
          </Link>
          <NavDropdown.Item onClick={() => logoutUser()}>Logout</NavDropdown.Item>
        </NavDropdown>
      </>
    );
  }

  return (
    <header className="bg-dark px-2">
      <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect className="py-3">
        <Container fluid style={{ maxWidth: '1400px' }}>
          <Link to="/">
            <Navbar.Brand>ZetaShop</Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Link to="/cart">
                <Nav.Link>
                  <i className="fas fa-shopping-cart"></i> Cart
                </Nav.Link>
              </Link>
              {authGroupButtons}
              {user && user?.role == 'ADMIN' && groupAdmin}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};
