export function truncate(words: string, num: number) {
  words.length > num ? words.substr(0, num) + '...' : words;
}
