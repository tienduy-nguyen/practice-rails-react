export const SERVER_LINKS = {
  authAutoRefresh: '/api/auth/auto-refresh',
  authLogin: '/api/auth/login', // POST with body {userOrEmail, password}
  authRegister: '/api/auth/register', // POST with body {username, email, password}
  authActivate: '/api/auth/activate', //GET with query ?token=
  authMe: '/api/auth/me', // GET
  authLogout: '/api/auth/logout',
};
