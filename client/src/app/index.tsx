import React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HomePage } from './pages/HomePage';
import { NotFoundPage } from './pages/NotFoundPage/loadable';

function App() {
  return (
    <BrowserRouter>
      <Helmet titleTemplate="%s - Training Session" defaultTitle="Training Session">
        <meta name="description" content="Training management system" />
      </Helmet>
      <header>Header</header>
      <div className="App">
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
      <footer>Footer</footer>
    </BrowserRouter>
  );
}

export default App;
