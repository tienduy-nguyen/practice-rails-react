import { combineReducers } from 'redux';
import persistReducer from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/es/storage';

const userPersistConfig = {
  key: 'user',
  storage,
  blacklist: ['errorMsg', 'successMsg'],
};
// Need update reducers
const userReducer: any = {};

export const rootReducers = combineReducers({
  // user: persistReducer(userPersistConfig, userReducer),
  // category: categoryReducer,
  // formation: formationReducer,
});

export type AppState = ReturnType<typeof rootReducers>;
