import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import persistStore from 'redux-persist/es/persistStore';
import { useDispatch } from 'react-redux';
import { rootReducers } from './reducers';

export const store = configureStore({
  reducer: rootReducers,
  middleware: [...getDefaultMiddleware()],
});

export const persistor = persistStore(store);
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
