import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import 'src/assets/styles/css/style.css';
import App from './app';
import { ToastContainer } from 'react-toastify';
import { Provider } from 'react-redux';
import { persistor, store } from './store';
import { PersistGate } from 'redux-persist/integration/react';
import { HelmetProvider } from 'react-helmet-async';

ReactDOM.render(
  // <Provider store={store}>
  // <PersistGate loading={null} persistor={persistor}>
  <HelmetProvider>
    <App />
    <ToastContainer />
  </HelmetProvider>,
  // </PersistGate>
  // </Provider>,
  document.getElementById('root'),
);
